var app = new Vue({ 
    el: "#app",
    data: {
        datos: [],
        turnoUser: [],
        propousers:[],
        form: {
            nombre: "",
            publicacion: "",
            imagen: ""
        },
        seen: true,
        estado: "crear",
        turn: false,
        idEdit: ""
    },
    methods: {
        getDatos() {
            this.seen = true;
            let url = route("datos.movie");
            axios.get(url).then(response => {
                console.log(response.data);
                this.datos = response.data;
            });
        },
        guardarMovie() {
            event.preventDefault();
            const params = {
                nombre: this.form.nombre,
                publicacion: this.form.publicacion,
                imagen: this.form.imagen
            };
            let url = route("crear.movie");
            axios.post(url, params).then((response) => {
                    Swal.fire(
                        "Exito!",
                        "La pelicula fue creadA con exito",
                        "success"
                    );
                    this.getDatos();
                }).catch((error) => {
                Swal.fire("Debes completar la informacion");
            });
        },
        eliminarMovie(dato) {
            Swal.fire({
                title: "¿Deseas Eliminar la pelicula?",
                showCancelButton: true,
                confirmButtonColor: "#2F3D4C",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si"
            }).then(result => {
                if (result.value) {
                    let url = route("eliminar.movie", dato.id);
                    axios.post(url).then((response) => {
                            Swal.fire(
                                "Eliminado!",
                                "El registro fue eliminado con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Ha ocurrido un error");
                    });
                }
            });
        },
        bloquearMovie(dato) {
            Swal.fire({
                title: "¿Deseas Bloquear la pelicula?",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si"
            }).then(result => {
                if (result.value) {
                    let url = route("bloquear.movie", dato.id);
                    axios.post(url).then((response) => {
                            Swal.fire(
                                "Bloqueado!",
                                "La pelicula fue bloqueada con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Ha ocurrido un error");
                    }); 
                }
            });
        },
        activarMovie(dato) {
            Swal.fire({
                title: "¿Deseas Activar la pelicula?",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si"
            }).then(result => {
                if (result.value) {
                    let url = route("activar.movie", dato.id);
                    axios.post(url).then((response) => {
                            Swal.fire(
                                "Activo!",
                                "La pelicula fue Activada con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Ha ocurrido un error");    
                    });
                }
            });
        },
        editarMovie(dato) {
            let url = route("mostrar.movie", dato.id);
            axios.get(url).then(response => {
                    this.idEdit = response.data.id;
                    this.form.nombre   = response.data.name;
                    this.form.publicacion   = response.data.date;
                    this.estado = "edit";
                })
                .catch((error) => {
                Swal.fire("Ha ocurrido un error"); 
                });
        },
        editMovie(id) {
            event.preventDefault();
            const params = {
                nombre: this.form.nombre,
                publicacion: this.form.publicacion,
                imagen: this.form.imagen
            };
            let url = route("edit.movie", id);
            axios.post(url, params).then((response) => {
                    Swal.fire(
                        "Exito!",
                        "La pelicula fue modificada con exito",
                        "success"
                    );
                    this.getDatos();
                }).catch((error) => {
                Swal.fire("Debes completar la informacion");
            });
        },
        turnMovie(dato) {
            event.preventDefault();
            let url = route("turnar.movie", dato.id);
            axios.get(url).then((response) => {
                    this.idEdit = response.data.id;
                    this.form.nombre = response.data.name;
                })
                .catch((error) => {
                Swal.fire("Ha ocurrido un error"); 
                });
        },
        getUser() {
            let url = route("consulta.usuario");
            axios.get(url).then(response => {
                console.log(response.data);
                this.propousers = response.data;
                
            });
        },
        getTurn() {
            let url = route("consulta.turno");
            axios.get(url).then(response => {
                console.log(response.data);
                this.turnoUser = response.data;
               
            });
        },

    },
    mounted() {
        this.getDatos();
    }
});
