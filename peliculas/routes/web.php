<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Cambio numero uno, probando.
/*Mi tarea era descargar dos proyectos del repositorio, modificar y subir los cambios, llevo 1.*/

Route::get('/', function () {
    return view('welcome');
});
// Segundo cambio

Route::get('/usuarios', 'PeliculaController@index')->name('index.pelicula');
Auth::routes();

Route::get('/movie', 'PeliculaController@index')->name('index.movie');
Route::get('/turn', 'TurnoController@index')->name('index.turno');
Route::get('/admin', 'AdminController@index')->name('index.admin');


Route::get('/extra', 'HomeController@index')->name('home');

Route::get('/melissa', 'HomeController@index')->name('home');
