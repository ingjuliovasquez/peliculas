<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Rutas para Movie 
Route::get('/datosmovie', 'PeliculaController@datosMovie')->name('datos.movie');
Route::post('/crearmovie', 'PeliculaController@crearMovie')->name('crear.movie');
Route::post('/bloquearmovie/{id}', 'PeliculaController@bloquearMovie')->name('bloquear.movie');
Route::post('/activarmovie/{id}', 'PeliculaController@activarMovie')->name('activar.movie');
Route::post('/eliminarmovie/{id}', 'PeliculaController@eliminarMovie')->name('eliminar.movie');
Route::get('/mostarmovie/{id}', 'PeliculaController@mostrarMovie')->name('mostrar.movie');
Route::post('/editarmovie/{id}', 'PeliculaController@editarMovie')->name('edit.movie');

//Rutas para Turnos
Route::get('/datosturn', 'TurnoController@datosTurn')->name('datos.turn');
Route::post('/crearturn', 'TurnoController@crearTurn')->name('crear.turn');
Route::post('/bloquearturn/{id}', 'TurnoController@bloquearTurn')->name('bloquear.turn');
Route::post('/activarturn/{id}', 'TurnoController@activarTurn')->name('activar.turn');
Route::post('/eliminarturn/{id}', 'TurnoController@eliminarTurn')->name('eliminar.turn');
Route::get('/mostarturn/{id}', 'TurnoController@mostrarTurn')->name('mostrar.turn');
Route::post('/editarturn/{id}', 'TurnoController@editarTurn')->name('edit.turn');

//Rutas para Admin
Route::get('/datosadmin', 'AdminController@datosAdmin')->name('datos.admin');

//Rutas para turnado
Route::get('/turnarmovie/{id}', 'PeliculaController@turnarMovie')->name('turnar.movie');
Route::get('/consultausuario', 'PeliculaController@getUsuarios')->name('consulta.usuario');
Route::get('/consultaturno', 'PeliculaController@getTurno')->name('consulta.turno');
Route::post('/crearturn', 'PeliculaController@crearTurno')->name('turnado');