<?php

use Illuminate\Database\Seeder;
use App\User;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('movies')->insert([
                
                ['name' => '10:30 am', 'date' => '2020-10-11', 'status_id' => 1], 
                ['name' => '11:30 am', 'date' => '2020-10-12','status_id' => 1], 
                ['name' => '12:30 am', 'date' => '2020-10-13','status_id' => 2], 
                ['name' => '13:30 am', 'date' => '2020-10-14','status_id' => 1]
            ]);


    }
}
