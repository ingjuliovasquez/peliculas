<?php

use Illuminate\Database\Seeder;
use App\User;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('status_movies')->insert([
                
                ['name' => 'ACTIVO'], 
                ['name' => 'INACTIVO']
            ]);


    }
}
