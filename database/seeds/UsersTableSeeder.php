<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year = date('Y');
        DB::table('users')->insert([
                
                ['name' => 'JULIO','first_name' => 'VÁSQUEZ','status' => 1,'email' => 'admin@admin.com','password' => bcrypt('temporal'.$year)],
                ['name' => 'ANA','first_name' => 'JULIAN','status' => 0,'email' => 'ana@ana.com','password' => bcrypt('temporal'.$year)],
                ['name' => 'FERNANDO','first_name' => 'DIAZ','status' => 0,'email' => 'fernando@fernando.com','password' => bcrypt('temporal'.$year)],
                ['name' => 'CARLOS','first_name' => 'ARLER','status' => 0,'email' => 'carlos@carlos.com','password' => bcrypt('temporal'.$year)],
                ['name' => 'MELISSA','first_name' => 'VASQUEZ','status' => 0,'email' => 'melissa@melissa.com','password' => bcrypt('temporal'.$year)],
            ]);


    }
}
