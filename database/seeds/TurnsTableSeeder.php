<?php

use Illuminate\Database\Seeder;
use App\User;

class TurnsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('turns')->insert([
                
                ['name' => '10:30 am', 'status_id' => 1], 
                ['name' => '11:30 am', 'status_id' => 1], 
                ['name' => '12:30 am', 'status_id' => 1], 
                ['name' => '13:30 am', 'status_id' => 1]
            ]);


    }
}
