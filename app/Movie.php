<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //
    public $table = 'movies';
    public $timestamps = false;
    public $fillable = [
        'id',
        'name',
        'date',
        'status_id'
       
    ];

    public function turn_movie(){
        return $this->hasMany('App\Turn_movie', 'movie_id', 'id');
    }

    public function status_movie(){
        return $this->belongsTo('App\Status_movie', 'status_id', 'id');
    }
}
// Prueba para cambio numero 15.