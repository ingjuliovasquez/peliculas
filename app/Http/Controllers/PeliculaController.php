<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\User;
use App\Turn;
use DB;

class PeliculaController extends Controller
{
    //

    public function index()
    {
        return view('pelicula.index');
    }

    public function datosMovie()
    {
        return Movie::with('status_movie')->get();
    }

    public function crearMovie(Request $request)
    {   
        try{
        $movie = new Movie;
        $movie->name = $request->nombre;
        $movie->date = $request->publicacion;
        $movie->status_id = 1;
        $movie->save();
        DB::commit();
        return response()->json([ /*'email'=>$user->email,*/ 'estatus'=>1]);
        }catch(Exception $e){
        DB::rollBack();
        return response()->json([ /*'email'=>$user->email,*/ 'estatus'=>2]);
        }
     
        
    }

    public function bloquearMovie($id)
    {
        try{
            $movie = Movie::find($id);
            $movie->status_id = 2;
            $movie->save();
            return Response()->json(['message' => 'success']);
        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function activarMovie($id)
    {
        try{
            $movie = Movie::find($id);
            $movie->status_id = 1;
            $movie->save();
            return Response()->json(['message' => 'success']);
        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function eliminarMovie($id)
    {
        try{
            Movie::find($id)->delete();
            return Response()->json(['message' => 'success']);
        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function mostrarMovie($id)
    {
        return Movie::find($id);
    }

    public function editarMovie(Request $request,$id)
    {
        try{
        $movie = Movie::find($id);
        $movie->name = $request->nombre;
        $movie->date = $request->publicacion;
        $movie->save();
        return Response()->json(['message' => 'success']);
        }catch(Exception $e){
        DB::rollBack();
        }
    }

    public function turnarMovie($id)
    {
        return Movie::find($id);
    }

    public function getUsuarios(){

        $user = User::all();
        return $user;
    }

    public function getTurno(){

        $turn = Turn::all();
        return $turn;
    }

    public function crearTurno(Request $request)
    {
        
    }
    
}
// Prueba para cambio siete.