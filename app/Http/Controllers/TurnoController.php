<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turn;
use DB;

class TurnoController extends Controller
{
    //
    
    public function index()
    {
        return view('turno.index');
    }

    public function datosTurn()
    {
        return Turn::with('status_movie')->get();
    }

    public function crearTurn(Request $request)
    {   
        try{
        $Turno = new Turn;
        $Turno->name = $request->publicacion;
        $Turno->status_id = 1;
        $Turno->save();
        DB::commit();
     
        }catch(Exception $e){
        DB::rollBack();
       
        }
     
        
    }

    public function bloquearTurn($id)
    {
        try{
            $Turno = Turn::find($id);
            $Turno->status_id = 2;
            $Turno->save();
            return Response()->json(['message' => 'success']);
        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function activarTurn($id)
    {
        try{
            $Turno = Turn::find($id);
            $Turno->status_id = 1;
            $Turno->save();
            return Response()->json(['message' => 'success']);
        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function eliminarTurn($id)
    {
        try{
            Turn::find($id)->delete();
            return Response()->json(['message' => 'success']);
        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function mostrarTurn($id)
    {
        return Turn::find($id);
    }

    
    public function editarTurn(Request $request,$id)
    {
        try{
        $Turno = Turn::find($id);
        $Turno->name = $request->publicacion;
        $Turno->save();
        return Response()->json(['message' => 'success']);
        }catch(Exception $e){
        DB::rollBack();
        }
    }
}
// Prueba para cambio numero 8.