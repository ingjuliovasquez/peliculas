<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// Segundo comentario en varios archivos.
class Status_movie extends Model
{
    //
    public $table = 'status_movies';
    public $timestamps = false;
    public $fillable = [
        'id',
        'name'
       
       
    ];

    public function turn(){
        return $this->hasOne('App\Turn', 'status_id', 'id');
    }

    public function movie(){
        return $this->hasOne('App\Movie', 'status_id', 'id');
    }
}
// Prueba para cambio numero 16