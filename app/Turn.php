<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turn extends Model
{
    //
    public $table = 'turns';
    public $timestamps = false;
    public $fillable = [
        'id',
        'name',
        'status_id'
        
        
    ];
    
    public function status_movie(){
        return $this->belongsTo('App\Status_movie', 'status_id', 'id');
    }

    public function turn_movie(){
        return $this->hasMany('App\Turn_movie', 'turn_id', 'id');
    }
}
// Prueba para cambio 18.