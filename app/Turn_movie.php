<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turn_movie extends Model
{
    //CAMBIO PARA MI TAREA NUMERO 1
    public $table = 'turn_movies';
    public $timestamps = false;
    public $fillable = [
        'id',
        'user_id',
        'movie_id',
        'turn_id',
        'date'
       
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function movie(){
        return $this->belongsTo('App\Movie', 'movie_id', 'id');
    }

    public function turn(){
        return $this->belongsTo('App\Movie', 'turn_id', 'id');
    }

}
// Prueba para cambio numero 17.