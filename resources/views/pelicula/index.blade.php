@extends('template.admin')
@section('page-css')
    <link href="{{ asset('plugins/Sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    
@endsection
@section('main-content')
<div id="app">
    <div class="row justify-content-center my-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h2 style="float: left;"><strong> PELICULAS </strong></h2>
                    <button type="button" class="agregar btn btn-primary btn-sm" style="float: right;" v-if="seen"
                            v-on:click="seen = false"><i class="i-Add-User mr-1" style="font-size:18px;"></i> Nuevo Pelicula 
                    </button>
                    <button type="button" class="agregar btn btn-primary btn-sm" style="float: right;" v-if="seen === false " 
                            v-on:click="seen = true"><i class="i-Add-User mr-1" style="font-size:18px;"></i> Lista Peliculas
                    </button>
                </div> 
                {{-- Tbla de PELICULAS--}}
                <div class="card-body" v-if="seen === true ">
                    <div class="body table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">Pelicula</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="dato in datos">
                                    <th scope="row">@{{dato.id}}</th>
                                    <td>@{{dato.name }}</td>
                                    <td>@{{dato.date }}</td>
                                    <td>@{{dato.status_movie.name}}</td>
                                    <td>
                                        <button type="button" class="btn btn-warnig btn-circle" v-on:click="seen = false" @click="editarMovie(dato)"><i class="i-Pen-5"></i></button>
                                        <button type="button" class="btn btn-success btn-circle" v-on:click="(turn = 'true') && (seen = 'true')" @click="turnMovie(dato)"><i class="i-Checked-User"></i></button>
                                        <button type="button" class="btn btn-primary btn-circle" @click="bloquearMovie(dato)" v-if="dato.status_movie.name === 'ACTIVO'" > <i class="i-File-Clipboard-File--Text"></i></button>
                                        <button type="button" class="btn btn-warning btn-circle" @click="activarMovie(dato)" v-else-if="dato.status_movie.name === 'INACTIVO'" ><i class="i-File-Clipboard-File--Text"></i></button>
                                        <button type="button" class="btn btn-danger btn-circle" @click="eliminarMovie(dato)" ><i class="nav-icon i-Close-Window"></i></button>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- Create & Edit de peliculas --}}
                <div v-if="seen === false">
                    <div class="card-body">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <form >
                                <label for="nombrePelicula" class="grey-text">Nombre</label>
                                <input type="text" id="nombrePelicula" class="form-control" name="form.nombre" v-model="form.nombre" />
                                <br/>
                                <label for="fechapPublicacion" class="grey-text">F. Publicación</label>
                                <input type="date" id="fechapPublicacion" value="" min="2018-01-01" max="2028-12-31" name="form.publicacion" v-model="form.publicacion">
                                <br/>
                                <div class="text-center mt-4">
                                    <button class="btn btn-primary" style="float: left;" type="submit"  v-if="estado === 'crear'" @click="guardarMovie">Guardar</button>
                                    <button class="btn btn-primary" style="float: left;" type="submit" v-if="estado === 'edit'" @click="editMovie(idEdit)">Guardar</button>
                                </div>
                                <br/>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- Turnado Peliculas--}}
                <div v-if="turn === 'true' && seen === 'true'">
                    <div class="card-body">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            {{-- <form > --}}
                                <label for="nombrePelicula" class="grey-text">Pelicula</label>
                                <input type="text" id="nombrePelicula" class="form-control" name="form.nombre" v-model="form.nombre" disabled />
                                <br/>
                                <label for="Usuario">Selecciona un Usuario</label>
                                <select name="Usuario" id="Usuario" class="form-control"   @click="getUser()">
                                   <option v-for="usuario in propousers">
                                       @{{usuario.name}}
                                    </option> 
                                </select>
                                <br/>
                                <label for="Turno">Selecciona un Turno</label>
                                <select name="Turno" id="Turno" class="form-control"   @click="getTurn()">
                                   <option v-for="Turno in turnoUser">
                                       @{{Turno.name}}
                                    </option> 
                                </select>
                                <br/>
                                <div class="text-center mt-4">
                                    <button class="btn btn-primary" style="float: left;" type="submit" @click="UserTurnar(idEdit,usuario.id,turno.id)" >Turnar</button>
                                    
                                </div>
                                <br/>
                            {{-- </form> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
    <script src=" https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js "> </script>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script src="{{ asset('plugins/Sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
    var app = new Vue({ 
    el: "#app",
    data: {
        datos: [],
        turnoUser: [],
        propousers:[],
        form: {
            nombre: "",
            publicacion: "",
            imagen: ""
        },
        seen: true,
        estado: "crear",
        turn: false,
        idEdit: ""
    },
    methods: {
        // Función para consultar pelicula
        getDatos() {
            this.seen = true;
            let url = route("datos.movie");
            axios.get(url).then(response => {
                console.log(response.data);
                this.datos = response.data;
            });
        },
        // Función para guardar pelicula
        guardarMovie() {
            event.preventDefault();
            const params = {
                nombre: this.form.nombre,
                publicacion: this.form.publicacion,
                imagen: this.form.imagen
            };
            let url = route("crear.movie");
            axios.post(url, params).then((response) => {
                    Swal.fire(
                        "Exito!",
                        "La pelicula fue creadA con exito",
                        "success"
                    );
                    this.getDatos();
                }).catch((error) => {
                Swal.fire("Debes completar la informacion");
            });
        },
        // Función para eliminar pelicula
        eliminarMovie(dato) {
            Swal.fire({
                title: "¿Deseas Eliminar la pelicula?",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si"
            }).then(result => {
                if (result.value) {
                    let url = route("eliminar.movie", dato.id);
                    axios.post(url).then((response) => {
                            Swal.fire(
                                "Eliminado!",
                                "El registro fue eliminado con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Ha ocurrido un error");
                    });
                }
            });
        },
        //Función para bloquear pelicula
        bloquearMovie(dato) {
            Swal.fire({
                title: "¿Deseas Bloquear la pelicula?",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si"
            }).then(result => {
                if (result.value) {
                    let url = route("bloquear.movie", dato.id);
                    axios.post(url).then((response) => {
                            Swal.fire(
                                "Bloqueado!",
                                "La pelicula fue bloqueada con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Ha ocurrido un error");
                    }); 
                }
            });
        },
        //Función para activar pelicula
        activarMovie(dato) {
            Swal.fire({
                title: "¿Deseas Activar la pelicula?",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si"
            }).then(result => {
                if (result.value) {
                    let url = route("activar.movie", dato.id);
                    axios.post(url).then((response) => {
                            Swal.fire(
                                "Activo!",
                                "La pelicula fue Activada con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Ha ocurrido un error");    
                    });
                }
            });
        },
        //Función para mostrar datos de editar pelicula
        editarMovie(dato) {
            let url = route("mostrar.movie", dato.id);
            axios.get(url).then(response => {
                    this.idEdit = response.data.id;
                    this.form.nombre   = response.data.name;
                    this.form.publicacion   = response.data.date;
                    this.estado = "edit";
                })
                .catch((error) => {
                Swal.fire("Ha ocurrido un error"); 
                });
        },
        // Función para editar pelicula
        editMovie(id) {
            event.preventDefault();
            const params = {
                nombre: this.form.nombre,
                publicacion: this.form.publicacion,
                imagen: this.form.imagen
            };
            let url = route("edit.movie", id);
            axios.post(url, params).then((response) => {
                    Swal.fire(
                        "Exito!",
                        "La pelicula fue modificada con exito",
                        "success"
                    );
                    this.getDatos();
                }).catch((error) => {
                Swal.fire("Debes completar la informacion");
            });
        },
        // Función para turnadpoo
        turnMovie(dato) {
            // event.preventDefault();
            let url = route("turnar.movie", dato.id);
            axios.get(url).then((response) => {
                    this.idEdit = response.data.id;
                    this.form.nombre = response.data.name;
                })
                .catch((error) => {
                Swal.fire("Ha ocurrido un error"); 
                });
        },
        getUser() {
            let url = route("consulta.usuario");
            axios.get(url).then(response => {
                console.log(response.data);
                this.propousers = response.data;
                
            });
        },
        getTurn() {
            let url = route("consulta.turno");
            axios.get(url).then(response => {
                console.log(response.data);
                this.turnoUser = response.data;
               
            });
        },
            UserTurnar(pe,usu,tu) {
                event.preventDefault();
                const params = {
                    nombre: pe,
                    publicacion: usu,
                    imagen: tu
                };
                console.log("Holaald",params);
                let url = route("turnado");
                axios.post(url, params).then((response) => {
                        Swal.fire(
                            "Exito!",
                            "Se ha Turnado con exito",
                            "success"
                        );
                    
                    }).catch((error) => {
                    Swal.fire("Debes completar la informacion");
                });
            },
    },
    mounted() {
        this.getDatos();
    }
});
</script>

@endsection
