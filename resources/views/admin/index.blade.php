@extends('template.admin')
@section('page-css')
    <link href="{{ asset('plugins/Sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('main-content')
<div id="app">
    <div class="row justify-content-center my-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h2 style="float: left;"><strong> Administradores </strong></h2>
            
                </div>
                <div class="card-body" >
                    <div class="body table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellido</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="dato in datos">
                                    <th scope="row">@{{dato.id}}</th>
                                    <td>@{{dato.name}}</td>
                                    <td>@{{dato.first_name}}</td>
                                    <td>@{{dato.email}}</td>
                                    <td>
                                    <button type="button" class="btn btn-danger btn-circle"  ><i class="nav-icon i-Close-Window"></i></button>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
    <script src=" https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js "> </script>
    <script src="{{ asset('plugins/Sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        var app = new Vue({ 
            el: "#app",
            data: {
                datos: [],
            },
            methods: {
                // Función para consultar Admin
                getDatos() {
                    let url = route("datos.admin");
                    axios.get(url).then(response => {
                        this.datos = response.data;
                    });
                },
            
            },
            mounted() {
                this.getDatos();
            }
        });

    </script>
@endsection
