@extends('template.admin')
@section('page-css')
    <link href="{{ asset('plugins/Sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('main-content')
<div id="app">
    <div class="row justify-content-center my-3">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h2 style="float: left;"><strong> TURNOS </strong></h2>
                    <button type="button" class="agregar btn btn-primary btn-sm" style="float: right;" v-if="seen"
                            v-on:click="seen = false"><i class="i-Add-User mr-1" style="font-size:18px;"></i> Nuevo Turno 
                    </button>
                    <button type="button" class="agregar btn btn-primary btn-sm" style="float: right;" v-if="seen === false" 
                            v-on:click="seen = true"><i class="i-Add-User mr-1" style="font-size:18px;"></i> Lista Turno
                    </button>
                </div>
                {{-- Tabla de turnos --}}
                <div class="card-body" v-if="seen === true">
                    <div class="body table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">Turno</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="dato in datos">
                                    <th scope="row">@{{dato.id}}</th>
                                    <td>@{{dato.name }}</td>
                                    <td>@{{dato.status_movie.name}}</td>
                                    <td>
                                        <button type="button" class="btn btn-warnig btn-circle" v-on:click="seen = false" @click="editarTurno(dato)"><i class="i-Pen-5"></i></button>
                                        <button type="button" class="btn btn-primary btn-circle" @click="bloquearTurno(dato)" v-if="dato.status_movie.name === 'ACTIVO'" > <i class="i-File-Clipboard-File--Text"></i></button>
                                        <button type="button" class="btn btn-warning btn-circle" @click="activarTurno(dato)" v-else-if="dato.status_movie.name === 'INACTIVO'" ><i class="i-File-Clipboard-File--Text"></i></button>
                                        <button type="button" class="btn btn-danger btn-circle" @click="eliminarTurno(dato)" ><i class="nav-icon i-Close-Window"></i></button>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div v-if="seen === false">
                    <div class="card-body">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <form>
                                <label for="fechapPublicacion" class="grey-text">Turno</label>
                                <input type="time" id="fechapPublicacion" value="" min="2018-01-01" max="2028-12-31" name="form.publicacion" v-model="form.publicacion">
                                <br/>
                                <label for="nombrePelicula" class="grey-text">Activo?</label>
                                <input type="checkbox" id="nombrePelicula" class="form-control" name="form.nombre" v-model="form.nombre" />
                                <br/>
                                <div class="text-center mt-4">
                                    <button class="btn btn-primary" style="float: left;" type="submit"  v-if="estado === 'crear'" @click="guardarTurno">Guardar</button>
                                    <button class="btn btn-primary" style="float: left;" type="submit" v-if="estado === 'edit'" @click="editTurno(idEdit)">Guardar</button>
                                </div>
                                <br/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
    <script src=" https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js "> </script>
    <script src="{{ asset('plugins/Sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        var app = new Vue({ 
            el: "#app",
            data: {
                datos: [],
                form: {
                    nombre: "",
                    publicacion: "",
                    imagen: "",
                },
                seen: true,
                estado: "crear",
                idEdit: ""
            },
            methods: {
                // Función para traer datos
                getDatos() {
                    this.seen = true;
                    let url = route("datos.turn");
                    axios.get(url).then(response => {
                        console.log(response.data);
                        this.datos = response.data;
                    });
                },
                // funcion para guardar turno
                guardarTurno() {
                    event.preventDefault();
                    const params = {
                        nombre: this.form.nombre,
                        publicacion: this.form.publicacion,
                        imagen: this.form.imagen
                    };
                    let url = route("crear.turn");
                    axios.post(url, params).then((response) => {
                            Swal.fire(
                                "Exito!",
                                "El turno fue creado con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Debes completar la informacion");
                    });
                },
                // Función para eliminar
                eliminarTurno(dato) {
                    Swal.fire({
                        title: "¿Deseas Eliminar el Turno?",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Si"
                    }).then(result => {
                        if (result.value) {
                            let url = route("eliminar.turn", dato.id);
                            axios.post(url).then((response) => {
                                    Swal.fire(
                                        "Eliminado!",
                                        "El registro fue eliminado con exito",
                                        "success"
                                    );
                                    this.getDatos();
                                }).catch((error) => {
                                Swal.fire("Ha ocurrido un error");
                            });
                        }
                    });
                },
                //Función para bloquear
                bloquearTurno(dato) {
                    Swal.fire({
                        title: "¿Deseas Bloquear el Turno?",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Si"
                    }).then(result => {
                        if (result.value) {
                            let url = route("bloquear.turn", dato.id);
                            axios.post(url).then((response) => {
                                    Swal.fire(
                                        "Bloqueado!",
                                        "El turno fue bloqueado con exito",
                                        "success"
                                    );
                                    this.getDatos();
                                }).catch((error) => {
                                Swal.fire("Ha ocurrido un error");
                            }); 
                        }
                    });
                },
                //Función para activar Turno
                activarTurno(dato) {
                    Swal.fire({
                        title: "¿Deseas Activar el Turno?",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Si"
                    }).then(result => {
                        if (result.value) {
                            let url = route("activar.turn", dato.id);
                            axios.post(url).then((response) => {
                                    Swal.fire(
                                        "Activo!",
                                        "El turno fue Activado con exito",
                                        "success"
                                    );
                                    this.getDatos();
                                }).catch((error) => {
                                Swal.fire("Ha ocurrido un error");    
                            });
                        }
                    });
                },
                //Función para editar mostrar datos
                editarTurno(dato) {
                    let url = route("mostrar.turn", dato.id);
                    axios.get(url).then(response => {
                            this.idEdit = response.data.id;
                            this.form.publicacion   = response.data.name;
                            
                            this.estado = "edit";
                        })
                        .catch((error) => {
                        Swal.fire("Ha ocurrido un error"); 
                        });
                },
                //Función para EditarTurno
                editTurno(id) {
                    event.preventDefault();
                    const params = {
                        nombre: this.form.nombre,
                        publicacion: this.form.publicacion,
                        
                    };
                    let url = route("edit.turn", id);
                    axios.post(url, params).then((response) => {
                            Swal.fire(
                                "Exito!",
                                "El turno fue modificado con exito",
                                "success"
                            );
                            this.getDatos();
                        }).catch((error) => {
                        Swal.fire("Debes completar la informacion");
                    });
                },
            },
            mounted() {
                this.getDatos();
            }
        });

    </script>
@endsection
