<!-- ============ Customizer ============= -->
<div class="customizer">
    <div class="handle" (click)="isOpen = !isOpen">
        <i class="i-Gear spin"></i>
    </div>
    <div class="customizer-body" data-perfect-scrollbar data-suppress-scroll-x="true">
        <div class="accordion" id="accordionCustomizer">
            <!-- <div class="card">
                <div class="card-header" id="headingOne">
                    <p class="mb-0">
                        Diseño de barra lateral
                    </p>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingThree"
                    data-parent="#accordionCustomizer">
                    <div class="card-body">

                        <div class="">
                            <a title="Compact Sidebar" href="#" class="btn btn-primary"> Compact
                                Sidebar </a>
                            <a title="Horizontal Layout" href="#" class="btn btn-primary">
                                Horizontal Layout </a>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="card">
                <div class="card-header" id="headingTwo">
                    <p class="mb-0">
                        Posición de barra lateral( RTL )
                    </p>
                </div>

                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionCustomizer">
                    <div class="card-body">
                        <label class="checkbox checkbox-primary">
                            <input type="checkbox" id="rtl-checkbox">
                            <span>Habilitar RTL</span>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>

            {{-- dark mode --}}
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <p class="mb-0">
                        Version nocturna
                    </p>
                </div>

                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionCustomizer">
                    <div class="card-body">
                        <label class="checkbox checkbox-primary">
                            <input type="checkbox" id="dark-checkbox">
                            <span>Habilitar modo nocturno</span>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
            {{-- <div class="card">
                    <div class="card-header" id="headingThree">
                        <p class="mb-0">
                            Bootstrap Colors
                        </p>
                    </div>

                    <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionCustomizer">
                        <div class="card-body">
                            <div class="bootstrap-colors colors">
                                <a title="lite-purple" class="color purple"> </a>
                                <a title="lite-blue" class="color blue"> </a>
                            </div>
                        </div>
                    </div>
                </div> --}}

        </div>
    </div>
</div>
<!-- ============ End Customizer ============= -->