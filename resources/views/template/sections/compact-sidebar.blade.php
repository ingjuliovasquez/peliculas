<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
    <ul class="navigation-left">
            <li class="nav-item {{ request()->is('inicio/*') ? 'active' : '' }}" data-item="inicio">
                <a class="nav-item-hold" href="{{ route('/') }}">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Inicio</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->is('buzon/*') ? 'active' : '' }}">
                <a class="nav-item-hold {{ Route::currentRouteName()=='buzon' ? 'open' : '' }}" href="{{ route('buzon') }}">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Buzón</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->is('carpeta/*') ? 'active' : '' }}" data-item="carpeta">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Library"></i>
                    <span class="nav-text">Carpetas</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item {{ request()->is('reporteador/*') ? 'active' : '' }}" data-item="reporteador">
                <a class="nav-item-hold" href="{{ route('reporteador.index') }}">
                    <i class="i-Pie-Chart-2"></i>
                    <span class="nav-text">Reporteador</span>
                </a>
                <div class="triangle"></div>
            </li>

           
        </ul>
    </div>

    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="carpeta">
            <li class="nav-item ">
                <a class=""
                    href="#">
                    <i class="nav-icon i-Clock-3"></i>
                    <span class="item-name">Iniciar Carpeta</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->