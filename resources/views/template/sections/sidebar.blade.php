
<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left menu">
           
            <li class="nav-item" >
                <a class="nav-item-hold ">
                    <i class="nav-icon i-Home1"></i>
                    <span class="nav-text">Inicio</span>
                </a>
                <div class="triangle"></div>
            </li>
       
            <li class="nav-item " >
                <a class="nav-item-hold" href="{{ url('/movie') }}" style="color:#5E3327;">
                    <i class="nav-icon i-Box-Full"></i>
                    <span class="nav-text">Peliculas</span>
                </a>
                <div class="triangle"></div>
            </li>    
            <li class="nav-item " >
                <a class="nav-item-hold" href="{{ url('/turn') }}" style="color:#5E3327;">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Turnos</span>
                </a>
                <div class="triangle"></div>
            </li>           

            <li class="nav-item " >
                <a class="nav-item-hold" href="{{ url('/admin') }}" style="color:#5E3327;">
                    <i class="nav-icon i-Split-Horizontal-2-Window"></i>
                    <span class="nav-text">Administradores</span>
                </a>
                <div class="triangle"></div>
            </li>           
      


            <li class="nav-item">
                <a class="nav-item-hold" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >
                    <i class="nav-icon i-Power-3"></i>
                    <span class="nav-text">Cerrar Sesión</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

        </ul>
    </div>


    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->
