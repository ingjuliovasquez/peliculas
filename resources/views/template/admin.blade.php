<!DOCTYPE html>
@routes
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>PELICULAS | Global STD</title>
        <link rel="icon" sizes="32x32" href="{{ asset('img/logo_global_hrz_color.png') }}">
        <link href="{{asset('plugins/gull/fonts/iconsmind/iconsmind.css')}}" rel="stylesheet">
        <link href="{{asset('plugins/gull/styles/css/themes/lite-purple.min.css')}}" id="gull-theme" rel="stylesheet" > 
       
         <!-- css icono -->
         <link rel="stylesheet" type="text/css" href="{{ asset('Plugins/fontawesome-free-5.11.2-web/css/fontawesome.min.css')}}">
         <link rel="stylesheet" type="text/css" href="{{ asset('Plugins/fontawesome-free-5.11.2-web/css/solid.css')}}">
 
        @yield('before-css')
        {{-- theme css --}}
   
        @stack('css')
        {{-- page specific css --}}
        @yield('page-css')

        <!-- vuejs -->
        <script src="{{  asset('gral/vue.js')}}"></script> 
        <script src="{{ asset('plugins/gull/js/vendor/jquery-3.3.1.min.js')}}"></script>
    </head>
    <body class="text-left">
        @php
        $layout = session('layout');
        @endphp

        <!-- Pre Loader Strat  -->
        <div class='loadscreen' id="preloader">

            <div class="loader spinner-bubble spinner-bubble-primary">


            </div>
        </div>
        <!-- Pre Loader end  -->
        <!-- ============ Compact Layout start ============= -->
        @if ($layout=="compact")

        <div class="app-admin-wrap layout-sidebar-compact sidebar-dark-purple sidenav-open clearfix">
            @include('template.sections.compact-sidebar')

            <!-- ============ end of left sidebar ============= -->


            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap d-flex flex-column">
                @include('template.sections.header-menu')

                <!-- ============ end of header menu ============= -->
                <div class="main-content">
                    @yield('main-content')
                </div>

                @include('template.sections.footer')
            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        <!-- include('template.search') -->
        <!-- ============ Search UI End ============= -->

        @include('template.sections.compact-customizer')



        <!-- ============ Compact Layout End ============= -->
        <!-- ============ Horizontal Layout start ============= -->

        @elseif($layout=="horizontal")

        <div class="app-admin-wrap layout-horizontal-bar clearfix">
            @include('template.sections.header-menu')

            <!-- ============ end of header menu ============= -->

            @include('template.sections.horizontal-bar')

            <!-- ============ end of left sidebar ============= -->

            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap  d-flex flex-column">
                <div class="main-content">
                    @yield('main-content')
                </div>

                @include('template.sections.footer')
            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        <!-- include('template.search') -->
        <!-- ============ Search UI End ============= -->

        @include('template.sections.horizontal-customizer')


        <!-- ============ Horizontal Layout End ============= -->

        <!-- ============ Large SIdebar Layout start ============= -->
        @elseif($layout=="normal")

        <div class="app-admin-wrap layout-sidebar-large clearfix">
            @include('template.sections.header-menu')

            <!-- ============ end of header menu ============= -->
            @include('template.sections.sidebar')

            <!-- ============ end of left sidebar ============= -->

            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap sidenav-open d-flex flex-column">
                <div class="main-content">
                    @yield('main-content')
                </div>

                @include('template.sections.footer')
            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        <!-- include('template.search') -->
        <!-- ============ Search UI End ============= -->

        @include('template.sections.large-sidebar-customizer')


        <!-- ============ Large Sidebar Layout End ============= -->

        @else
        <!-- ============Deafult  Large SIdebar Layout start ============= -->

        {{-- normal layout --}}
        <div class="app-admin-wrap layout-sidebar-large clearfix">
            @include('template.sections.header-menu')
            {{-- end of header menu --}}



            @include('template.sections.sidebar')
            {{-- end of left sidebar --}}

            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap sidenav-open d-flex flex-column">
                <div class="main-content">
                    @yield('main-content')
                </div>

                @include('template.sections.footer')
            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        <!-- include('template.search') -->
        <!-- ============ Search UI End ============= -->

        @include('template.sections.large-sidebar-customizer')


        <!-- ============ Large Sidebar Layout End ============= -->
        @endif


    @prepend('scripts')

        {{-- common js --}}
        <script src="{{  asset('plugins/gull/js/common-bundle-script.js')}}"></script>
        {{-- page specific javascript --}}
        @yield('page-js')

        {{-- theme javascript --}}
        {{-- <script src="{{mix('plugins/gull/js/es5/script.js')}}"></script> --}}
        <script src="{{asset('plugins/gull/js/script.js')}}"></script>


        @if ($layout=='compact')
        <script src="{{asset('plugins/gull/js/sidebar.compact.script.js')}}"></script>


        @elseif($layout=='normal')
        <script src="{{asset('plugins/gull/js/sidebar.large.script.js')}}"></script>


        @elseif($layout=='horizontal')
        <script src="{{asset('plugins/gull/js/sidebar-horizontal.script.js')}}"></script>


        @else
        <script src="{{asset('plugins/gull/js/sidebar.large.script.js')}}"></script>

        @endif

        <script src="{{asset('plugins/gull/js/vendor/sweetalert2.min.js')}}"></script>
        

        <script src="{{asset('plugins/gull/js/customizer.script.js')}}"></script>
        <script src="{{asset('plugins/gull/js/vendor/toastr.min.js')}}"></script>



        <script src="{{ asset('gral/select2.min.js')}}"></script>
  
        <script src="{{ asset('plugins/moment/moment.min.js')}}"></script>

    @endprepend

    </body>

    @stack('scripts')

    @yield('bottom-js')
</html>
